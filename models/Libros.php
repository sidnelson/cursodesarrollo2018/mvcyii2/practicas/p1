<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id_libro
 * @property string $titulo
 * @property string $foto
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_libro'], 'required'],
            [['id_libro'], 'integer'],
            [['titulo'], 'string', 'max' => 255],
            [['foto'], 'string', 'max' => 50],
            [['id_libro'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_libro' => 'Id Libro',
            'titulo' => 'Titulo',
            'foto' => 'Foto',
        ];
    }
}
