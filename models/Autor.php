<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property int $id_autor
 * @property string $titulo
 * @property string $foto
 */
class Autor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_autor'], 'required'],
            [['id_autor'], 'integer'],
            [['titulo'], 'string', 'max' => 255],
            [['foto'], 'string', 'max' => 50],
            [['id_autor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_autor' => 'Id Autor',
            'titulo' => 'Titulo',
            'foto' => 'Foto',
        ];
    }
}
