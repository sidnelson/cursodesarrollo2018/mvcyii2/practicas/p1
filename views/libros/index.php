<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\BaseDataProvider;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <!-- 
    <p>
        <?php /* Html::a('Create Libros', ['create'], ['class' => 'btn btn-success']) */?>
    </p>
   -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => "Mostrando {begin} - {end} de {totalCount} libros",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'titulo',
            'foto' => [
                'label'=>'Foto',
                'format'=>'raw',
                'value' => function($data){
                    return Html::img("@web/imgs/libros/$data->foto", ['width' => '150px']);
                }
            ],
                
                
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  
</div>
