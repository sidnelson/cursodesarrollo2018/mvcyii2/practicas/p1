<?= 
    $this->render('_campo',[
        "valor"=>$model->titulo,
        "etiqueta"=>$model->getAttributeLabel("titulo"),
    ]);
?>
<?= 
    $this->render('_campo',[
        "valor"=>\yii\helpers\Html::img("@web/imgs/autores/$model->foto",[
        'class'=>'img-responsive img-rounded w-2 center-block'
    ]),
        "etiqueta"=>$model->getAttributeLabel("foto"),
    ]);
?>
